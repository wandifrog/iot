import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.css'

import VueRouter from 'vue-router'
import Home from "./components/Home.vue";
import Page2 from "./components/Page2.vue";

import axios from 'axios'

const routes = [
  { path: '/', component: Home },
  { path: '/channel/:id', component: Page2 }
  // { path: '/page2', component: Page2 }
]
const router = new VueRouter({
  routes,
})

Vue.use(axios)
Vue.use(VueRouter)
Vue.use(Vuetify)

new Vue({
  el: '#app',
  render: h => h(App),
  router
})
